﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Design;
using System.ComponentModel;

namespace wifitest
{
    public class CSetting : arUtil.Setting
    {
        [Category("연결"), Editor(typeof(MyUITypeEditor), typeof(UITypeEditor))]
        public string APName { get; set; }

        [Category("연결"), DisplayName("AP접속암호"), PasswordPropertyText(true)]
        public string Password { get; set; }
        [Category("연결"), DisplayName("최소신호세기(%)")]
        public int Strength { get; set; }
        [Category("연결"), DisplayName("AP자동 접속 간격")]
        public int Interval { get; set; }


        [Category("기능"), DisplayName("강제 연결 끊기"), Description("Wifi 가 연결되어 있다면 연결을 끊고 다시 시도 합니다")]
        public Boolean AutoDisconnect { get; set; }

        [Category("기능"), DisplayName("자동 연결"), Description("프로그램 실행시 자동으로 연결 합니다")]
        public Boolean AutoRunConnect { get; set; }

        [Category("기능"), DisplayName("자동 종료"), Description("연결 완료시 프로그램을 종료 합니다")]
        public Boolean AutoClose { get; set; }

        [Category("알림(해제)"), DisplayName("접속종료")]
        public Boolean Disable_Notify_Disconnect { get; set; }
        [Category("알림(해제)"), DisplayName("접속시도")]
        public Boolean Disable_Notify_ConnectTry { get; set; }
        [Category("알림(해제)"), DisplayName("접속완료")]
        public Boolean Disable_Notify_ConnectOK { get; set; }
        [Category("알림(해제)"), DisplayName("접속실패")]
        public Boolean Disable_Notify_ConnectFail { get; set; }

        [Category("알림(해제)"), DisplayName("접속정보오류")]
        public Boolean Disable_Notify_NoAPSetting { get; set; }
        [Category("알림(해제)"), DisplayName("AP검색실패")]
        public Boolean Disable_Notify_NoAPList { get; set; }
        [Category("알림(해제)"), DisplayName("신호세기실패")]
        public Boolean Disable_Notify_NoAPPower { get; set; }

        public CSetting()
        {

        }
        public override void AfterLoad()
        {
            if (Interval == 0) Interval = 10;
            if (Strength == 0) Strength = 50;
        }
        public override void AfterSave()
        {

        }
    }

    class MyUITypeEditor : UITypeEditor
    {
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            var f = new fAPList();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return f.APName;
            }
            else return base.EditValue(context, provider, value);
        }
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }
}
