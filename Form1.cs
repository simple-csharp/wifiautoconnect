﻿using SimpleWifi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wifitest
{
    public partial class Form1 : Form
    {
        SimpleWifi.Wifi wifi = new SimpleWifi.Wifi();
        public Form1()
        {
            InitializeComponent();
            Pub.Init();
            this.FormClosed += Form1_FormClosed;
            this.FormClosing += Form1_FormClosing;
            this.Shown += Form1_Shown;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            //this.notifyIcon1.Visible = !this.Visible;
        }

        Boolean bclose = false;
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            timer1.Stop();
            DiposeNotify();
        }

        void ShowMode(Boolean visible)
        {
            if(visible==false)
            {
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false; 
               // this.Visible = false;
               // this.Hide();
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.ShowInTaskbar = true;
                if(this.timer1.Enabled == true)
                {
                    this.timer1.Stop();
                    this.timer1.Enabled = false;
                    NotifyMsg("자동 연결이 해제 되었습니다");
                }
                
               // this.Visible = true;
               // this.Show();
                //this.Activate();
                
            }
            Application.DoEvents();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //속성 연결
            this.tbName.Text = Pub.setting.APName;
            this.tbPw.Text = Pub.setting.Password;
            this.Show();
            Application.DoEvents();


            if (Pub.setting.AutoRunConnect)
            {
                if (Pub.setting.APName != "")
                {
                    
                     btConn.PerformClick();
                }
                else
                {
                    NotifyMsg("AP Name 이 없어 자동 연결이 되지 않습니다");
                }
            }
            else
            {
                if (Pub.setting.APName.isEmpty() == true)
                    btSetup.PerformClick();
            }

        
        }

        private AccessPoint GetAPbyName(string apName)
        {
            AccessPoint retval = null;
            var aps = wifi.GetAccessPoints();
            foreach (var ap in aps)
            {
                if (ap.Name == apName)
                {
                    retval = ap;
                    break;
                }
            }
            return retval;
        }

        DateTime conntry = DateTime.Now;    
      
        public  bool connectToWifi(AccessPoint ap, string password)
        {
            if (ap != null)
            {
                AuthRequest authRequest = new AuthRequest(ap);
                authRequest.Password = password;
                try
                {
                    while (!ap.Connect(authRequest, true))
                    {
                        ap.Connect(authRequest);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return true;
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Validate();

            btConn.Enabled = false;
            if(Pub.setting.Interval == 0)  
                Pub.setting.Interval = 10;
          
            if(tbName.Text != Pub.setting.APName || 
                tbPw.Text != Pub.setting.Password )
            {
                Pub.setting.APName = tbName.Text;
                Pub.setting.Password = tbPw.Text;
                Pub.setting.Save();
                Pub.log.Add("설정 저장");
            }

            
            if (wifi.ConnectionStatus == WifiStatus.Connected)
            {
                if(Pub.setting.AutoDisconnect)
                {
                    NotifyMsg("기존 연결을 종료 합니다");
                    wifi.Disconnect();
                }
                else
                {
                    NotifyMsg("현재 Wifi 가 연결 중이므로 접속을 시도하지 않습니다", ToolTipIcon.Warning);
                    btConn.Enabled = true;
                    return;
                }
                
            }



            if (Connect(false) == true)
            {
                bConnect = true;
                if (Pub.setting.AutoClose)
                {
                    bclose = true;
                    this.Close();
                }
            }
            else
            {
                NotifyMsg("접속 실패(" + Pub.setting.APName + ")", ToolTipIcon.Error);
                bConnect = false;
            }

            btConn.Enabled = true;
        }

        bool Connect(bool autoMode)
        {
            if(Pub.setting.APName.isEmpty() == true)
            {
               // Pub.log.AddAT("접속 AP 이름이 설정되지 않았음");
                if(autoMode == false || Pub.setting.Disable_Notify_NoAPSetting == false) NotifyMsg("AP Name 이 입력되지 않았습니다");
                return false;
            }

            var ap = GetAPbyName(Pub.setting.APName);
            if(ap == null)
            {
               // Pub.log.AddAT("지정된 AP가 검색되지 않음");
                if (autoMode == false || Pub.setting.Disable_Notify_NoAPList == false) NotifyMsg("AP가 검색되지 않습니다(" + Pub.setting.APName + ")");
                bConnect = false;
                return false;
            }
            else
            {

                if (ap.SignalStrength <= Pub.setting.Strength)
                {
                    if (autoMode == false || Pub.setting.Disable_Notify_NoAPPower == false)
                        NotifyMsg("AP(" + Pub.setting.APName + ") 의 신호세기가 부족합니다. 기준(" + Pub.setting.Strength.ToString() + "%)", ToolTipIcon.Error);
                    bConnect = false;
                    return false;
                }


                if (autoMode == false || Pub.setting.Disable_Notify_ConnectTry == false) NotifyMsg("다음 AP에 접속을 시도 합니다\n" + Pub.setting.APName);
                if (connectToWifi(ap, Pub.setting.Password) == true)
                {
                    bConnect = true;
                    if ( Pub.setting.Disable_Notify_ConnectOK == false) NotifyMsg("접속 성공(" + Pub.setting.APName + ")");
                }
                else
                {
                    Pub.log.AddAT("AP 접속 실패");
                    if (autoMode == false || Pub.setting.Disable_Notify_ConnectFail == false) NotifyMsg("접속 실패(" + Pub.setting.APName + ")", ToolTipIcon.Error);
                    bConnect = false;
                }
            }
            return bConnect;
        }
        private void ShowAPList()
        {
            var f = new fAPList();
            if (f.ShowDialog() == DialogResult.OK)
            {
                tbName.Text = f.APName;
            }
        }

        public  void NotifyMsg(string m, System.Windows.Forms.ToolTipIcon icontype = System.Windows.Forms.ToolTipIcon.Info)
        {
            if (notifyIcon1 == null)
            {
                notifyIcon1 = new System.Windows.Forms.NotifyIcon();
                notifyIcon1.Icon = Properties.Resources.icons8_wi_fi_logo_16_ico;
                notifyIcon1.Visible = true;
            }
            notifyIcon1.ShowBalloonTip(1000, "Wifi auto Connect", m, icontype);
            
        }
        public  void DiposeNotify()
        {
            if (notifyIcon1 != null)
            {
                notifyIcon1.Visible = false;
                notifyIcon1.Dispose();
            }
        }

        Boolean tmRun = false;
        Boolean bConnect = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //화면에 보이고 있다면 처리 하지 않는다. 보이고 있는것은 설정 중이다
            if (tmRun == true || this.WindowState == FormWindowState.Normal) return;
            tmRun = true;

            var ts = DateTime.Now - conntry;

            if (bConnect == true && wifi.ConnectionStatus == WifiStatus.Disconnected)
            {
                NotifyMsg("연결이 종료되었습니다. 잠시 후 다시 시도 합니다");
                conntry = DateTime.Now.AddSeconds(-1 * (Pub.setting.Interval ));
                bConnect = false;
            }

            //아직 시간이 되지 않았다면 연결을 시도하지 않음
            if (ts.TotalSeconds <= Pub.setting.Interval)
            {
                tmRun = false;
                return;
            }

         
            //if (Pub.setting.APName == "")
            //{
            //    NotifyMsg("AP 정보가 없어 접속을 할 수 없습니다", ToolTipIcon.Error);
            //    tmRun = false;
            //    return;
            //}
            //var ap = GetAPbyName(Pub.setting.APName);
            //if (ap == null)
            //{
            //    NotifyMsg("AP(" + Pub.setting.APName + ") 이 존재하지 않습니다", ToolTipIcon.Error);
            //    tmRun = false;
            //    return;
            //}

           

            //NotifyMsg(string.Format("AP({0}) 에 접속을 시도 합니다", Pub.setting.APName));

            if ( wifi.ConnectionStatus == WifiStatus.Disconnected)
            {
               
                this.Connect(true);
              
            }
                   conntry = DateTime.Now;
            tmRun = false;
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            ShowMode(true);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var f = new fSetup();
            if(f.ShowDialog() ==  DialogResult.OK)
            {
                tbName.Text = Pub.setting.APName;
                tbPw.Text = Pub.setting.Password;
            }
        }

        private void btMon_Click(object sender, EventArgs e)
        {
            
            if(tbName.Text.isEmpty()==true)
            {
                NotifyMsg("AP Name 이 입력되지 않아 자동연결을 실행할 수 없습니다");
                return;
            }

            this.conntry = DateTime.Now;
            NotifyMsg("자동 연결이 설정되었습니다. " + Pub.setting.Interval.ToString() + "초 마다 연결을 시도합니다");
            timer1.Start();
            ShowMode(false);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ShowAPList();
        }
    }
}
