﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace wifitest
{
    /// <summary>
    /// generic method Extension
    /// </summary>
    public static class MethodExtensions
    {
        public static string ToString(this System.Drawing.Rectangle rect)
        {
            return string.Format("X={0},Y={1},W={2},H={3}", rect.X, rect.Y, rect.Width, rect.Height);
        }
        public static string ToString(this System.Drawing.RectangleF rect)
        {
            return string.Format("X={0},Y={1},W={2},H={3}", rect.X, rect.Y, rect.Width, rect.Height);
        }

        //public static void SetBGColor(this System.Windows.Forms.Label ctl,System.Drawing.Color color1)
        //{
        //    ctl.BackColor = System.Drawing.Color.Red;
        //}

        /// <summary>
        /// 0101이 반복되는 문자열 형태로 전환합니다.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static string BitString(this System.Collections.BitArray arr)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = arr.Length; i > 0; i--)
                sb.Append(arr[i - 1] ? "1" : "0");
            return sb.ToString();
        }

        /// <summary>
        /// int 값으로 변환합니다.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static int ValueI(this System.Collections.BitArray arr)
        {
            byte[] buf = new byte[4];
            arr.CopyTo(buf, 0);
            return BitConverter.ToInt32(buf, 0);
        }

        /// <summary>
        /// 숫자인지 검사합니다.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string input)
        {
            double data;
            return double.TryParse(input, out data);
            //return Regex.IsMatch(input, @"^\d+$");
        }

        /// <summary>
        /// isnullorempty 를 수행합니다.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Boolean isEmpty(this string input)
        {
            return string.IsNullOrEmpty(input);
        }

        /// <summary>
        /// default 인코딩을 사용하여 문자열로 반환합니다.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetString(this Byte[] input)
        {
            return System.Text.Encoding.Default.GetString(input);
        }

        /// <summary>
        /// 16진수 문자열 형태로 반환합니다.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetHexString(this Byte[] input)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (byte b in input)
                sb.Append(" " + b.ToString("X2"));
            return sb.ToString();
        }

        public static string Base64Encode(this string src)
        {
            string base64enc = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(src));
            return base64enc;
        }
        public static string Base64Decode(this string src)
        {
            var base64dec = Convert.FromBase64String(src);
            return System.Text.Encoding.UTF8.GetString(base64dec);
        }
        
        public static string toSizeStr(this Size rect)
        {
            return string.Format("{0};{1}", rect.Width, rect.Height);
        }
        public static string toPointStr(this PointF rect)
        {
            return string.Format("{0};{1}", rect.X, rect.Y);
        }
        public static string toPointStr(this Point rect)
        {
            return string.Format("{0};{1}", rect.X, rect.Y);
        }
        public static string toRectStr(this Rectangle rect)
        {
            return string.Format("{0};{1};{2};{3}", rect.X, rect.Top, rect.Width, rect.Height);
        }
        public static string toRectStr(this RectangleF rect)
        {
            return string.Format("{0};{1};{2};{3}", rect.X, rect.Top, rect.Width, rect.Height);
        }
        public static RectangleF toRectF(this string str)
        {
            var buffer = str.Split(';');
            if (buffer.Length != 4) return RectangleF.Empty;
            for (int i = 0; i < buffer.Length; i++)
                if (buffer[i] == "") buffer[i] = "0";
            return new RectangleF(float.Parse(buffer[0]), float.Parse(buffer[1]), float.Parse(buffer[2]), float.Parse(buffer[3]));
        }
        public static Rectangle toRect(this string str)
        {
            var buffer = str.Split(';');
            if (buffer.Length != 4) return Rectangle.Empty;
            for (int i = 0; i < buffer.Length; i++)
                if (buffer[i] == "") buffer[i] = "0";
            return new Rectangle(int.Parse(buffer[0]), int.Parse(buffer[1]), int.Parse(buffer[2]), int.Parse(buffer[3]));
        }
        public static Point toPoint(this string str)
        {
            str = str.Replace(",", ";").Replace("*", ";").Replace("x", ";").Replace(":", ";");

            var buffer = str.Split(';');
            if (buffer.Length != 2) return Point.Empty;
            for (int i = 0; i < buffer.Length; i++)
                if (buffer[i] == "") buffer[i] = "0";
            return new Point(int.Parse(buffer[0]), int.Parse(buffer[1]));
        }
        public static Size toSize(this string str)
        {
            str = str.Replace(",", ";").Replace("*", ";").Replace("x", ";").Replace(":", ";");

            var buffer = str.Split(';');
            if (buffer.Length != 2) return Size.Empty;
            for (int i = 0; i < buffer.Length; i++)
                if (buffer[i] == "") buffer[i] = "0";
            return new Size(int.Parse(buffer[0]), int.Parse(buffer[1]));
        }
        public static PointF toPointF(this string str)
        {
            str = str.Replace(",", ";").Replace("*", ";").Replace("x", ";").Replace(":", ";");

            var buffer = str.Split(';');
            if (buffer.Length != 2) return PointF.Empty;
            for (int i = 0; i < buffer.Length; i++)
                if (buffer[i] == "") buffer[i] = "0";
            return new PointF(float.Parse(buffer[0]), float.Parse(buffer[1]));
        }
        public static Rectangle Fix(this Rectangle rect, Size imgsize)
        {
            //영역보정
            if (rect.Left < 1) rect.X = 1;
            if (rect.Top < 1) rect.Y = 1;
            if (rect.Right > imgsize.Width) rect.Width = imgsize.Width - rect.Left - 1;
            if (rect.Bottom > imgsize.Height) rect.Height = imgsize.Height - rect.Top - 1;
            if (rect.Width < 2 || rect.Height < 2) rect = Rectangle.Empty;
            return rect;
        }
        public static RectangleF FixRoi(this RectangleF rect, Size imgsize)
        {
            //영역보정
            if (rect.Left < 1) rect.X = 1;
            if (rect.Top < 1) rect.Y = 1;
            if (rect.Right > imgsize.Width) rect.Width = imgsize.Width - rect.Left - 1;
            if (rect.Bottom > imgsize.Height) rect.Height = imgsize.Height - rect.Top - 1;
            if (rect.Width < 2 || rect.Height < 2) rect = Rectangle.Empty;
            return rect;
        }

    }
}

