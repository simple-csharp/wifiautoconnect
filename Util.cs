﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace wifitest
{
    public static class Util
    {
        #region "MessageBox"
        public static void MsgI(string m)
        {
           MessageBox.Show(m, "확인", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void MsgE(string m)
        {
       
            MessageBox.Show(m, "오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static DialogResult MsgQ(string m,string btOK = "확인",string btCancel = "아니오")
        {
           DialogResult dlg = MessageBox.Show(m, "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            return dlg;
        }

        #endregion

        public static void ScreenCaptrue(int _BitmapWidth, int _BitmapHeight, Point ptSouce)
        {
            try
            {
                Bitmap bitmap = new Bitmap(_BitmapWidth, _BitmapHeight);
                Graphics _graphic = Graphics.FromImage(bitmap);

                _graphic.CopyFromScreen(ptSouce, new Point(0, 0), new Size(_BitmapWidth, _BitmapHeight));

                string savefile = System.IO.Path.Combine(Util.CurrentPath, "ScreenShot", DateTime.Now.ToString("yyyyMMddHHmmss") + ".png");
                System.IO.FileInfo grpath = new FileInfo(savefile);
                if (!grpath.Directory.Exists) grpath.Directory.Create();

                bitmap.Save(grpath.FullName, System.Drawing.Imaging.ImageFormat.Png);
                Util.MsgI("화면 캡쳐 성공\n" + 
                    "위치:" + grpath.Directory.FullName +"\n" +
                    "파일명: " + grpath.Name);
            }
            catch (Exception ex)
            {
                Util.MsgE("화면 캡쳐 실패\n" + ex.Message);
            }
        }

                
        public static void SaveBugReport(string content, string subdirName = "BugReport")
        {
            try
            {
                var path = CurrentPath + subdirName;
                if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);
                var file = path + "\\" + DateTime.Now.ToString("yyyyMMdd_HHmmss_fff") + ".txt";
                System.IO.File.WriteAllText(file, content, System.Text.Encoding.UTF8);
            }
            catch
            {
                //nothing
            }
        }

        public static void CopyData(System.Data.DataRow drSrc, System.Data.DataRow drDes)
        {
            if (drDes == null || drSrc == null) return;
            foreach (System.Data.DataColumn col in drSrc.Table.Columns)
            {
                if (col.ColumnName.ToUpper() == "IDX") continue;    
                drDes[col.ColumnName] = drSrc[col.ColumnName];
            }
                
        }

        /// <summary>
        /// 현재실행중인폴더를 반환합니다.
        /// </summary>
        public static string CurrentPath
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }
        /// <summary>
        /// 콤마와 줄바꿈등을 제거합니다.
        /// </summary>
        /// <returns></returns>
        public static string ToCSVString(string src)
        {
            string retval = src.Replace("\r", "").Replace("\n", "").Replace(",", "");
            return retval;
        }

        public static Boolean RunProcess(string file, string arg = "")
        {
            var fi = new System.IO.FileInfo(file);
            if (!fi.Exists)
            {
                Pub.log.AddE("Run Error : " + file);
                return false;
            }
            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo(file);
            si.Arguments = arg;
            prc.StartInfo = si;
            prc.Start();
            return true;
        }

        #region "convert"
        public static string RectToStr(Rectangle rect)
        {
            return string.Format("{0};{1};{2};{3}", rect.X, rect.Y, rect.Width, rect.Height);
        }
        public static string RectToStr(RectangleF rect)
        {
            return string.Format("{0};{1};{2};{3}", rect.X, rect.Y, rect.Width, rect.Height);
        }
        public static string PointToStr(Point pt)
        {
            return string.Format("{0};{1}", pt.X, pt.Y);
        }
        public static string PointToStr(PointF pt)
        {
            return string.Format("{0};{1}", pt.X, pt.Y);
        }
        public static Rectangle StrToRect(string str)
        {
            if (str.isEmpty() || str.Split(';').Length != 4) str = "0;0;0;0";
            var roibuf1 = str.Split(';');
            return new System.Drawing.Rectangle(
                int.Parse(roibuf1[0]),
                int.Parse(roibuf1[1]),
                int.Parse(roibuf1[2]),
                int.Parse(roibuf1[3]));
        }
        public static RectangleF StrToRectF(string str)
        {
            if (str.isEmpty() || str.Split(';').Length != 4) str = "0;0;0;0";
            var roibuf1 = str.Split(';');
            return new System.Drawing.RectangleF(
                float.Parse(roibuf1[0]),
                float.Parse(roibuf1[1]),
                float.Parse(roibuf1[2]),
                float.Parse(roibuf1[3]));
        }
        public static Point StrToPoint(string str)
        {
            if (str.isEmpty() || str.Split(';').Length != 2) str = "0;0";
            var roibuf1 = str.Split(';');
            return new System.Drawing.Point(
                int.Parse(roibuf1[0]),
                int.Parse(roibuf1[1]));
        }
        public static PointF StrToPointF(string str)
        {
            if (str.isEmpty() || str.Split(';').Length != 2) str = "0;0";
            var roibuf1 = str.Split(';');
            return new System.Drawing.PointF(
                float.Parse(roibuf1[0]),
                float.Parse(roibuf1[1]));
        }
        #endregion

        #region "NIC"

        /// <summary>
        /// 지정된 nic카드가 현재 목록에 존재하는지 확인한다.
        /// </summary>
        /// <returns></returns>
        public static Boolean ExistNIC(string NICName)
        {
            if (string.IsNullOrEmpty(NICName)) return false;
            foreach (string NetName in NICCardList())
            {
                if (NetName.ToLower() == NICName.ToLower())
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Ehternet Card 를 사용안함으로 설정합니다.(관리자권한필요)
        /// </summary>
        /// <param name="NicName"></param>
        public static Boolean NICDisable(string NICName)
        {
            //해당 nic 가 현재 목록에 존재하는지 확인한다.

            string cmd = "interface set interface " + NICName + " disable";
            Process prc = new Process();
            ProcessStartInfo si = new ProcessStartInfo("netsh", cmd);
            si.WindowStyle = ProcessWindowStyle.Hidden;
            prc.StartInfo = si;
            prc.Start();

            ////목록에서 사라질때까지 기다린다.
            DateTime SD = DateTime.Now;
            Boolean timeout = false;
            while ((true))
            {

                bool FindNetwork = false;
                foreach (string NetName in NICCardList())
                {
                    if (NetName == NICName.ToLower())
                    {
                        FindNetwork = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

                if (!FindNetwork)
                    break; // TODO: might not be correct. Was : Exit While

                System.Threading.Thread.Sleep(1000);
                TimeSpan ts = DateTime.Now - SD;
                if (ts.TotalSeconds > 10)
                {
                    timeout = true;
                    break; // TODO: might not be correct. Was : Exit While
                }
            }
            return !timeout;
        }

        public static List<String> NICCardList()
        {
            List<String> Retval = new List<string>();
            foreach (System.Net.NetworkInformation.NetworkInterface Net in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                if (Net.NetworkInterfaceType == System.Net.NetworkInformation.NetworkInterfaceType.Ethernet)
                {
                    Retval.Add(Net.Name.ToUpper());
                }
            }
            return Retval;
        }

        /// <summary>
        /// 이더넷카드를 사용함으로 설정합니다.
        /// </summary>
        /// <param name="NicName"></param>
        public static Boolean NICEnable(string NICName)
        {
            string cmd = "interface set interface " + NICName + " enable";
            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo("netsh", cmd);
            si.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            prc.StartInfo = si;
            prc.Start();


            ////목록에생길떄까지 대기
            DateTime SD = DateTime.Now;
            while ((true))
            {

                bool FindNetwork = false;
                foreach (string NetName in NICCardList())
                {
                    if (NetName.ToLower() == NICName.ToLower())
                    {
                        FindNetwork = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

                if (FindNetwork)
                    break; // TODO: might not be correct. Was : Exit While

                System.Threading.Thread.Sleep(1000);
                TimeSpan ts = DateTime.Now - SD;
                if (ts.TotalSeconds > 10)
                {
                    return false;
                }
            }

            ////결이 완료될떄까지 기다린다.
            SD = DateTime.Now;
            while ((true))
            {

                bool FindNetwork = false;
                foreach (System.Net.NetworkInformation.NetworkInterface Net in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (Net.NetworkInterfaceType != System.Net.NetworkInformation.NetworkInterfaceType.GigabitEthernet &&
                        Net.NetworkInterfaceType != System.Net.NetworkInformation.NetworkInterfaceType.Ethernet) continue;
                    if (Net.Name.ToLower() == NICName.ToLower())
                    {
                        //string data = Net.GetIPProperties().GatewayAddresses[0].ToString();

                        if (Net.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up)
                        {

                            FindNetwork = true;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                }
                if (FindNetwork)
                    return true;

                System.Threading.Thread.Sleep(1000);
                TimeSpan ts = DateTime.Now - SD;
                if (ts.TotalSeconds > 10)
                {
                    return false;
                }
            }

        }

        #endregion

        public static void RunExplorer(string arg)
        {
            System.Diagnostics.ProcessStartInfo si = new ProcessStartInfo("explorer");
            si.Arguments = arg;
            System.Diagnostics.Process.Start(si);
        }

        #region "watchdog"
        public static void WatchDog_Run()
        {
            System.IO.FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "WatchCat.exe");
            if (!fi.Exists) return;
            var Exist = CheckExistProcess("watchcat");
            if (Exist) return;
            RunProcess(fi.FullName);
        }

        public static Boolean CheckExistProcess(string ProcessName)
        {
            foreach (var prc in System.Diagnostics.Process.GetProcesses())
            {
                if (prc.ProcessName.StartsWith("svchost")) continue;
                if (prc.ProcessName.ToUpper() == ProcessName.ToUpper()) return true;
            }
            return false;
        }
        #endregion

        #region "web function"
        /// <summary>
        /// URL로부터 문자열을 수신합니다.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="isError"></param>
        /// <returns></returns>
        public static string GetStrfromurl(string url, out Boolean isError)
        {
            isError = false;
            string result = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.Timeout = 60000;
                request.ReadWriteTimeout = 60000;

                request.MaximumAutomaticRedirections = 4;
                request.MaximumResponseHeadersLength = 4;
                request.Credentials = CredentialCache.DefaultCredentials;
                var  response  = request.GetResponse() as HttpWebResponse;
                var txtReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                result = txtReader.ReadToEnd();
            }
            catch (Exception ex)
            {
                isError = true;
                result = ex.Message.ToString();
            }
            return result;
        }
        
        #endregion

    }
}
