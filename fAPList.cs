﻿using SimpleWifi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wifitest
{
    public partial class fAPList : Form
    {
        public string APName { get; set; }
        public fAPList()
        {
            InitializeComponent();
            this.listView1.DoubleClick += ListView1_DoubleClick;
        }

        private void ListView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.FocusedItem == null) return;
            this.APName = listView1.FocusedItem.Text;
            DialogResult = DialogResult.OK;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var wifi = new SimpleWifi.Wifi();
            var aps = wifi.GetAccessPoints();
            foreach (var ap in aps)
            {
                if (string.IsNullOrWhiteSpace(ap.Name.Trim())) continue;
                var lv = this.listView1.Items.Add(ap.Name);
                lv.SubItems.Add(ap.SignalStrength.ToString() + "%");
                lv.Tag = ap;
            }
        }

    }
}
