﻿using SimpleWifi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace wifitest
{
    public partial class fSetup : Form
    {
        SimpleWifi.Wifi wifi = new SimpleWifi.Wifi();
        public fSetup()
        {
            InitializeComponent();
        }

      
        private void Form1_Load(object sender, EventArgs e)
        {
            //속성 연결
            this.propertyGrid1.SelectedObject = Pub.setting;
         
        }

        private AccessPoint GetAPbyName(string apName)
        {
            AccessPoint retval = null;
            var aps = wifi.GetAccessPoints();
            foreach (var ap in aps)
            {
                if (ap.Name == apName)
                {
                    retval = ap;
                    break;
                }
            }
            return retval;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            this.Validate();

            if(Pub.setting.Interval == 0)
            {
                Util.MsgI("접속 재시도 간격(초)을 10초로 기본 설정 합니다");
                Pub.setting.Interval = 10;
            }

          
            Pub.setting.Save();
            Pub.log.Add("설정 저장");
            DialogResult = DialogResult.OK;
        }

        private void ShowAPList()
        {
            var f = new fAPList();
            if (f.ShowDialog() == DialogResult.OK)
            {
                Pub.setting.APName = f.APName;
                this.propertyGrid1.Refresh();
            }
        }

       
    }
}
